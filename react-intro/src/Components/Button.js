import React from "react";
import PropTypes from "prop-types";
import "./Styles/Button.css";

class Button extends React.Component {


  render() {
    //1) define className here with all the options
    //2) apply the class name to the divs accordingly to the props orange / wide
    //3) create a funct named handleClick and apply to the button click event
    //4) the text of the button is received through the props "name"
    return (
      <div > 
        <button></button>
      </div>
    );
  }
}
Button.propTypes = {
  name: PropTypes.string, // button text
  orange: PropTypes.bool, //color is orange or not?
  wide: PropTypes.bool, //is the button wider or not?
  clickHandler: PropTypes.func, //hey, user clicked the button, do something with it parent... i will pass you the name that you sent-me just in case 
};
export default Button;