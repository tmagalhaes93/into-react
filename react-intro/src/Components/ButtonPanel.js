import Button from "./Button";
import React from "react";
import PropTypes from "prop-types";

import "./Styles/ButtonPanel.css";

class ButtonPanel extends React.Component {

  //1) create a funct named handleClick and apply to the button click event with the button name as prop
  //2) look at button, find the props that you should send for each... TIP-> you should pass the function to apply the click event :)
  render() {
    return (
      <div></div>
      // <div className="component-button-panel">
      //   <div>
      //   <Button name="" />
      //   <Button name="" />
      //   <Button name="" />
      //   <Button name="" />
      //   </div>
      //   <div>
      //   <Button name="" />
      //   <Button name="" />
      //   <Button name="" />
      //   <Button name="" />

      //   </div>
      //   <div>
      //   <Button name="" />
      //   <Button name="" />
      //   <Button name="" />
      //   <Button name="" />

      //   </div>
      //   <div>
      //   <Button name="" />
      //   <Button name="" />
      //   <Button name="" />
      //   <Button name="" />
      //   </div>
      //   <div>
      //   <Button name="" />
      //   <Button name="" />
      //   <Button name="" />
      //   </div>
      // </div>
    );
  }
}
ButtonPanel.propTypes = {
  clickHandler: PropTypes.func, //hey, user clicked the button, do something with it parent... i will pass you the name that you sent-me just in case 
};
export default ButtonPanel;