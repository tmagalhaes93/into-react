import React from "react";
import PropTypes from "prop-types";

import "./Styles/Display.css";

class Display extends React.Component {
  //1- there you go... another stateless component
  //2- create a div inside the already defined div and set the received props as content :) 
  render() {
    return (
      <div className="component-display">
      </div>
    );
  }
}
Display.propTypes = {
  value: PropTypes.string,
};
export default Display;